﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace async_and_await
{
    class Program
    {
        public static async void print_id_thread()
        {
            await Task.Run(() => Console.WriteLine( "ID  асинхронного  потока " + Thread.CurrentThread.ManagedThreadId));
        }
        static void Main(string[] args)
        {
            Thread thread = Thread.CurrentThread;
            Console.WriteLine("ID  главного потока " + thread.ManagedThreadId);
            print_id_thread();
                        
            Console.ReadLine();
            
        }
    }
}
