﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace ClientServerApp
{
    class Program
    {
        const int Port = 11000;
        const int Backlog = 1000;

        static void Main(string[] args)
        {
            try
            {
                Run();
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);                
            }
            Console.ReadKey();
        }

        public static void  acc(object socket)
        {

            while (true)
            {
                Socket handler = ((Socket)socket).Accept();
                byte[] buffer_for_size = new byte[4];
                handler.Receive(buffer_for_size, 0, 4, SocketFlags.None);
                int bufferSize = BitConverter.ToInt32(buffer_for_size, 0);
                byte[] buffer = new byte[bufferSize];
                int numberRecievedBytes = handler.Receive(buffer, 0, bufferSize, SocketFlags.None);
                var result = Encoding.ASCII.GetString(buffer, 0, bufferSize);
                Console.WriteLine(result);
            }
        }

        private static void Run()
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(endPoint);
            socket.Listen(Backlog);
            new Thread(new ParameterizedThreadStart(acc)).Start(socket);
        }

    }
}
