﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace Client
{
    class Program
    {
        const int Port = 11000;

        static void Main(string[] args)
        {
            try
            {
                Run();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadKey();
        }
        public static void connect_soccet()
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);
            string s = "ID Tread: " + Thread.CurrentThread.ManagedThreadId;
            
            byte[] buffer_size = BitConverter.GetBytes(s.Length);
            socket.Send(buffer_size, 0, buffer_size.Length, SocketFlags.None);
            
            byte[] buffer = Encoding.ASCII.GetBytes(s);
            int numberReadBytes = socket.Send(buffer, 0, buffer.Length, SocketFlags.None);
            
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
        private static void Run()
        {
            string hostNmae = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostNmae);
            IPAddress address = hostEntry.AddressList.First();
            IPEndPoint endPoint = new IPEndPoint(address, Port);
            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);
            
            for (int i = 0; i < 90; i++)
                new Thread(new ThreadStart(connect_soccet)).Start();                
            
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}
